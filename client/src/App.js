import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';

import logo from './logo.svg';
import Home from './pages/Home';
import ContactUs from './pages/ContactUs';
import './App.css';

const App = () => {
    return (
        <div className="app">
            <header className="app-header">
                <div className="app-logo">
                    <img src={logo} alt="logo" width="100" />
                </div>
                <nav>
                    <ul className="navigation">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/contact">Contact Us</Link>
                        </li>
                    </ul>
                </nav>
            </header>

            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/contact' component={ContactUs} />
            </Switch>
        </div>
    );
}

export default App;
