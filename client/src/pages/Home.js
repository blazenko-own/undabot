import React from 'react';

import './Home.css';

const Home = () => {

    return (
        <section>
            <div className="text">
                <h1>Arena, Pula</h1>
                <p>The Pula Arena (Croatian: <em>Pulska Arena</em>, Italian: <em>Arena di Pola</em>) is the name of the
                    amphitheatre located in Pula, Croatia. The Arena is the only remaining Roman amphitheatre to have
                    four side towers entirely preserved. It was constructed in 27 BC – 68 AD and is among the world's six
                    largest surviving Roman arenas. A rare example among the 200 surviving Roman amphitheatres,
                    it is also the best preserved ancient monument in Croatia; however, the arena is not listed
                    on UNESCO world heritage list.
                </p>
                <p>
                    The Arena was built between 27 BC and 68 AD, as the city of Pula became a regional centre
                    of Roman rule, called Pietas Julia. The name was derived from the sand that, since antiquity,
                    covered the inner space. It was built outside the town walls along the Via Flavia, the road
                    from Pula to Aquileia and Rome.
                </p>
                <p>
                    The amphitheatre was first built in timber during the reign of Augustus (2–14 AD). It was replaced
                    by a small stone amphitheatre during the reign of emperor Claudius. In 79 AD it was enlarged to
                    accommodate gladiator fights by Vespasian and to be completed in 81 AD under emperor Titus.
                    This was confirmed by the discovery of a Vespasian coin in the malting.
                </p>
            </div>

            <picture>
                <source srcSet="arena.jpg" media="(min-width: 769px)" />
                <source srcSet="arena-768.jpg" media="(min-width: 361px)" />
                <source srcSet="arena-360.jpg" media="(max-width: 360px)" />
                <img src="arena.jpg" width={1450} alt="Night shot of Arena" />
            </picture>
        </section>
    );
};

export default Home;
