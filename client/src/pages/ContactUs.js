import React, { useState } from 'react';

import './ContactUs.css';

const ContactUs = () => {
    const [email, setEmail] = useState('');
    const [emailDirtyClass, setEmailDirtyClass] = useState('');
    const [msg, setMsg] = useState('');
    const [msgDirtyClass, setMsgDirtyClass] = useState('');
    const [responseStr, setResponseStr] = useState('');
    const [feedbackErrorClass, setFeedbackErrorClass] = useState('');

    const onEmailChange = (e) => {
        const content = e.target.value;
        setEmail(content);
        setEmailDirtyClass(content === '' ? '' : 'dirty');
    };

    const onMsgChange = (e) => {
        const content = e.target.value;
        setMsg(content);
        setMsgDirtyClass(content === '' ? '' : 'dirty');
    };

    const postData = async (body) => {
        const response = await fetch('/API/contact', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body
        });
        setFeedbackErrorClass(response.status === 200 ? '' : 'error');
        return response.json();
    };

    const onSubmit = () => {
        const body = JSON.stringify({ email, msg });
        postData(body).then(res => setResponseStr(res.message));
    };

    return (
        <div className="contact-us">
            <h1>Contact Us</h1>
            <div className="elm-row">
                <input
                    id="email"
                    value={email}
                    className={`form-elm email-field ${emailDirtyClass}`}
                    onChange={onEmailChange}
                />
                <label htmlFor="email">Email</label>
            </div>

            <div className="elm-row">
                <textarea
                    id="msg"
                    value={msg}
                    className={`form-elm msg-field ${msgDirtyClass}`}
                    rows={7}
                    onChange={onMsgChange}
                />
                <label htmlFor="msg">Message</label>
            </div>

            <div className="elm-row">
                <button
                    type="button"
                    className="form-elm submit-button"
                    onClick={onSubmit}
                    aria-label="Submit button"
                >
                    Send
                </button>
            </div>

            <div className="elm-row">
                <pre className={`feedback ${feedbackErrorClass}`}>
                    { responseStr }
                </pre>
            </div>
        </div>
    );
};

export default ContactUs;
