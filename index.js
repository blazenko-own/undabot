const express = require('express');
const path = require('path');

const app = express();

app.use(express.json());

app.use(express.static(path.join(__dirname, 'client/build')));

app.post('/API/contact', (req,res) => {
    const email = req.body.email;
    const msg = req.body.msg;

    const emailErr = checkEmail(email) ? '' : 'Email is not valid!';
    const msgErr = checkMsg(msg) ? '' : 'Message is shorter than 30 characters!'

    const status = checkEmail(email) && checkMsg(msg) ? 200 : 422;
    const response = { message: status === 200 ? 'Your message has been sent!' : `${emailErr}\n${msgErr}` };
    res.status(status).json(response);
});

app.get('*', (req,res) => {
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = 5000;
app.listen(port);

console.log('App is listening on port ' + port);

function checkEmail(email) {
    const rx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    return rx.test(email);
}

function checkMsg(msg) {
    return msg.length >= 30;
}
